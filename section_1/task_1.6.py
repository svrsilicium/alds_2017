# -*- coding: UTF-8 -*-

"""
Реализуйте метод для выполнения простейшего сжатия строк с использованием счетчика
повторяющихся символов. Например, строка aabcccccaaa превращается в a2b1c5a3. Если
'сжатая' строка не становится короче исходной, то метод возвращает исходную строку.
Предполагается, что строка состоит только из букв верхнего и нижнего регистров (a-z).
92+, 110+
"""


# ~50 min,  O(n)

def store(new_str, char, count):
    # something like StringBuilder from Java
    # faster than new_str += new_str + char + str(count)
    string_builder = list()
    string_builder.append(new_str)
    string_builder.append(char)
    string_builder.append(str(count))
    return ''.join(string_builder)


def compress_string(string):
    size = len(string)
    if size <= 2:            # ---> O(n) or even O(1)
        return string

    count = 1
    new_str = ""

    for i in range(1, size):            # ---> O(n)
        if string[i] == string[i - 1]:
            count += 1
        else:
            # storing previous sequence
            new_str = store(new_str, string[i - 1], count)
            count = 1

    # storing last sequence
    new_str = store(new_str, string[size - 1], count)

    if len(new_str) == size:
        return string
    else:
        return new_str


print("in : {:s}".format("a"))
print("out: {:s}\n".format(compress_string("a")))

print("in : {:s}".format("aa"))
print("out: {:s}\n".format(compress_string("aa")))

print("in : {:s}".format("aabbcc"))
print("out: {:s}\n".format(compress_string("aabbcc")))

print("in : {:s}".format("aabbcccddeffffa"))
print("out: {:s}\n".format(compress_string("aabbcccddeffffa")))

print("in : {:s}".format("oaabbcccddeffffa"))
print("out: {:s}\n".format(compress_string("oaabbcccddeffffa")))
