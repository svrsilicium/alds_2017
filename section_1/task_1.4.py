# -*- coding: UTF-8 -*-

"""
Напишите функцию, которая проверяет, является ли заданная строка
перестановкой палиндрома. Палиндром - слово или фраза, одинаково
читающаяся в прямом и обратном направлении; перестановка - строка,
содержащая те же символы в другом порядке. Палиндром не
ограничивается словами из словаря.
106+, 121+, 134+, 136
"""


# ~30 min   O(N)

def permuted_palindrome(string):
    string = string.lower().replace(" ", "")			# ---> O(N)
    if len(string) == 1:								# ---> O(N)
        return True
    elif len(string) == 0:								# ---> O(N)
        return False

    ascii_dict = {chr(i): 0 for i in range(255)}		# ---> O(1)

    for x in string:									# ---> O(N)
        ascii_dict[x] += 1								# ---> O(1)

    odd_count = 0
    for x in ascii_dict:								# ---> O(N)
        if ascii_dict[x] % 2 == 1:
            odd_count += 1
        if odd_count > 1:
            return False
    return True


print(permuted_palindrome("Tact Coa"))      # True
print(permuted_palindrome("abc bAc r"))     # True
print(permuted_palindrome(" "))             # False
print(permuted_palindrome("Abc Cba abc"))   # False


