# -*- coding: UTF-8 -*-

"""
Для двух строк напишите метод, определяющий,
является ли одна строка перестановкой другой.
Подсказки: 1, 84, 122, 131
"""


# first solution ~10 min  O(N^2)
def count_x(x, string):
    count = 0
    for i in range(len(string)):
        if string[i] == x:
            count += 1
    return count


def shuffled_strings(string_1, string_2):
    if len(string_1) != len(string_2):
        return False

    for x in string_1:
        count = count_x(x, string_1)
        if count != count_x(x, string_2):
            return False
    return True


string_1 = 'abab'
string_2 = 'abba'
print(shuffled_strings(string_1, string_2))     # True

string_1 = 'ababc'
string_2 = 'abbay'
print(shuffled_strings(string_1, string_2))     # False


# second solution ~20 min  O(N)
def shuffled_strings(string_1, string_2):
    if len(string_1) != len(string_2):
        return False

    ascii_dict = {chr(i): 0 for i in range(255)}    # hash_table

    for x in string_1:
        ascii_dict[x] += 1

    for x in string_2:
        ascii_dict[x] -= 1

    for x in ascii_dict:
        if ascii_dict[x] != 0:
            return False
    return True


string_1 = 'abab'
string_2 = 'abba'
print(shuffled_strings(string_1, string_2))     # True

string_1 = 'ababc'
string_2 = 'abbay'
print(shuffled_strings(string_1, string_2))     # False
