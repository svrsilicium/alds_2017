# -*- coding: UTF-8 -*-

"""
Существуют три вида модифицирующих операций со строками: вставка символа,
удаление символа и замена символа. Напишите функцию, которая проверяет,
находятся ли две строки на расстоянии одной модификации (или нуля модификаций).
23+, 97+, 130
"""


# ~40 min,  O(n)

def compare_neighbours(string_1, string_2):     # ---> O(n)
    for i in range(len(string_1)):
        if string_1[i] == string_2[i]:
            pass
        elif string_1[i] == string_2[i + 1]:
            pass
        else:
            return False
    return True


def one_or_null_modification(string_1, string_2):
    if abs(len(string_1) - len(string_2)) >= 2:     # ---> O(n)
        return False
    
    # deletion or addition  
    if len(string_1) < len(string_2):
        return compare_neighbours(string_1, string_2)   # ---> O(n)
    elif len(string_2) < len(string_1):
        return compare_neighbours(string_2, string_1)   # ---> O(n)

    # replacement or False
    if len(string_1) == len(string_2):
        diff_count = 0
        for i in range(len(string_1)):                          # ---> O(n)
            if string_1[i] == string_2[i]:
                pass
            else:
                diff_count += 1
            if diff_count > 1:
                return False
        return True
    else:
        return False
          
            
print(one_or_null_modification("brake", "bbrake"))     # True
print(one_or_null_modification("brake", "rake"))       # True

print(one_or_null_modification("rbrake", "brake"))     # True
print(one_or_null_modification("rake", "brake"))       # True

print(one_or_null_modification("brake", "drake"))      # True

print(one_or_null_modification("pale", "ple"))       # True
print(one_or_null_modification("pales", "pale"))     # True
print(one_or_null_modification("pale", "bale"))      # True
print(one_or_null_modification("pale", "bake"))      # False

