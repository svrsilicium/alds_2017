# -*- coding: UTF-8 -*-

"""
Реализуйте алгоритм, определяющий, все ли символы в строке
встречаются только один раз. А если при этом запрещено
использование дополнительных структур данных?
Подсказки: 44, 117, 132
"""

string_1 = "somestring"
string_2 = "abcdefgh"


# first solution, ~15 min
def are_all_unique_1(string):
    for i, x in enumerate(string):
        if string[i + 1:].find(x) >= 0:
            return False
    return True


print(are_all_unique_1(string_1))   # False
print(are_all_unique_1(string_2))   # True


# second solution, ~ 10 min
def is_in_substring(x, string):
    for i in string:
        if i == x:
            return True
        else:
            return False


def are_all_unique_2(string):
    for i, x in enumerate(string):
        if is_in_substring(x, string[i + 1:]):
            return False
    return True


print(are_all_unique_2(string_1))   # False
print(are_all_unique_2(string_2))   # True

print(are_all_unique_2("5789114226"))   # False
print(are_all_unique_1("5789114226"))   # False
print(are_all_unique_1("6639998841"))   # False
