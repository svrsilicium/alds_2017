# -*- coding: UTF-8 -*-

"""
Напишите программу сортировки стека, в результате которой наименьший
элемент оказывается на вершине стека. Вы можете использовать
дополнительный временный стек, но элементы не должны копироваться
в другие структуры данных (например, в массив). Стек должен поддерживать
следующие операции: push, pop, peek, isEmpty.
Подсказки: 15, 32, 43
"""


class Data:
    def __init__(self, value: int=None):
        self.value = value


class Stack:
    def __init__(self):
        self.items = []

    def pop(self):
        return self.items.pop()

    def push(self, item: Data):
        self.items.append(item)

    def peek(self):
        if self.is_empty():
            return None
        else:
            return self.items[-1]

    def is_empty(self):
        return self.items == []

    def __str__(self):
        if not self.items:
            return "Stack []"
        else:
            return str([x.value for x in self.items])


def _is_lower(item_1: Data, item_2: Data):
    if item_1.value < item_2.value:
        return True
    else:
        return False


def algorithm(s: Stack):
    t = Stack()
    while not s.is_empty():
        temp = s.pop()
        while not t.is_empty() and _is_lower(temp, t.peek()):
            s.push(t.pop())
        t.push(temp)
    return t


r1 = Stack()
r1.push(Data(3))
r1.push(Data(5))
r1.push(Data(1))
r1.push(Data(4))
r1.push(Data(2))
r1.push(Data(1))
r1.push(Data(8))
r1.push(Data(4))


print(r1)
a = algorithm(r1)
print(a)
