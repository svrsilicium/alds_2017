# -*- coding: UTF-8 -*-

"""
Как реализовать стек, в котором кроме стандартных функций push и pop будет
поддерживаться функция min, возвращающая минимальный элемент? Все операции -
push, pop и min - должны выполняться за время O(1).
27+, 59+, 78+
"""


# ~30 min,  O(1) for all methods,   memory O(n) (2n in reality)
# Used special Data type instead of simple type (int ot whatever) to show logic generally.

class Data:
    def __init__(self, value=None):
        self.value = value


class Node:
    def __init__(self, data=None, minimum=None):
        self.data = data
        self.minimum = minimum


class Stack:
    def __init__(self):
        self.items = []

    def pop(self):
        return self.items.pop()

    @staticmethod
    def _is_lower(item_1, item_2):
        if item_1.data.value < item_2.data.value:
            return True
        else:
            return False

    def push(self, data):
        item = Node(data)
        if self.is_empty():
            item.minimum = item
        else:
            minimum = self.items[-1].minimum
            if self._is_lower(item, minimum):
                item.minimum = item
            else:
                item.minimum = minimum
        self.items.append(item)

    def peek(self):
        return self.items[-1]

    def is_empty(self):
        return self.items == []

    def min(self):
        return self.items[-1].minimum


r = Stack()
r.push(Data(3))
r.push(Data(6))
r.push(Data(5))
r.push(Data(4))

print("Minimum is: {:d}".format(r.min().data.value))
