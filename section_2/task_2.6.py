# -*- coding: UTF-8 -*-

"""
Реализуйте функцию, проверяющую, является ли связный список палиндромом.
5, 13, 29, 61, 101
"""


# ~25 min,  O(n),   memory O(n) - in reality O(n/2)

class Node:
    def __init__(self, value=None, next=None):
        self.value = value
        self.next = next


class LinkedList:
    def __init__(self):
        self.first = None
        self.last = None
        self.length = 0
        self.index = 0

    def clear(self):
        self.__init__()

    def add(self, value):
        self.length += 1
        if self.first is None:
            self.first = self.last = Node(value)
        else:
            self.last.next = self.last = Node(value)

    def __str__(self):
        if self.length > 0:
            current = self.first
            out = "LinkedList [" + str(current.value)
            while current.next is not None:
                current = current.next
                out += ", " + str(current.value)
            return out + "]"
        else:
            return "LinkedList []"

    def __iter__(self):
        return self

    def __next__(self):
        if self.index == self.length:
            raise StopIteration
        self.index += 1


def is_palindrome(lst):
    if lst.length == 0:
        return False

    stack = list()
    current = lst.first
    middle = 1 if lst.length % 2 == 1 else 0

    for i in range(lst.length):
        if i < lst.length // 2:
            stack.append(current.value)
        elif i >= lst.length // 2 + middle:
            if current.value == stack.pop():
                pass
            else:
                return False
        current = current.next
    return True


def to_linked_list(data, lst):
    for x in data:
        lst.add(x)


t = LinkedList()
to_linked_list("1234321", t)
print(is_palindrome(t))         # True

f = LinkedList()
to_linked_list("none", f)
print(is_palindrome(f))         # False
