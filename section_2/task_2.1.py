# -*- coding: UTF-8 -*-

"""
Напишите код для удаления дубликатов из несортированного связного списка.
Доролнительно
Как вы будете решать задачу, если использовать временный буфер запрещено?
9+, 40
"""


# ~1 hr,    O(n^2), memory O(1)

class Node:
    def __init__(self, value=None, next=None):
        self.value = value
        self.next = next


class LinkedList:
    def __init__(self):
        self.first = None
        self.last = None
        self.length = 0

    def __str__(self):
        if self.first is not None:
            current = self.first
            out = "LinkedList [" + str(current.value)
            while current.next is not None:
                current = current.next
                out += ", " + str(current.value)
            return out + "]"
        return "LinkedList []"

    def clear(self):
        self.__init__()

    def add(self, other):
        self.length += 1
        if self.first is None:
            self.first = self.last = Node(other, None)
        else:
            self.last.next = self.last = Node(other, None)

    def delete_duplicate(self):     # ---> O(n^2), memory O(1)
        current = self.first
        while current.next is not None:
            runner = current
            while runner.next is not None:
                if runner.next.value == current.value:
                    runner.next = runner.next.next
                else:
                    runner = runner.next
            current = current.next


L = LinkedList()
L.add(10)
L.add(20)
L.add(42)
L.add(23)
L.add(9)
L.add(42)
L.add(17)
L.add(10)
L.add(10)

print(L)
L.delete_duplicate()
print(L)
