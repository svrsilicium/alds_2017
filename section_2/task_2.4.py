# -*- coding: UTF-8 -*-

"""
Напишите код для разбиения связного списка вокруг значения х, так чтобы все узлы,
меньшие х, предшествовали узлам, большим или равным х. Если х содержится в списке,
то значения х должны следовать строго после элементов, меньших х (см.далее). Элемент
разбивки х может находиться где угодно в 'правой части'; он не обязан располагаться
между левой и правой частью.
3+, 24+
"""


# ~1 hr,    O(n),   memory O(1)

class Node:
    def __init__(self, value=None, next=None):
        self.value = value
        self.next = next


class LinkedList:
    def __init__(self):
        self.first = None
        self.last = None
        self.length = 0

    def add(self, value):
        self.length += 1
        if self.first is None:
            self.first = self.last = Node(value)
        else:
            self.last.next = self.last = Node(value)

    def clear(self):
        self.__init__()

    def __str__(self):
        if self.length > 0:
            current = self.first
            out = "LinkedList [" + str(current.value)
            while current.next is not None:
                current = current.next
                out += ", " + str(current.value)
            return out + "]"
        else:
            return "LinkedList []"

    def sift(self, x):
        current = self.first
        previous = None
        for i in range(self.length):            # ---> O(n)
            if current.value < x and i > 0:
                previous.next = current.next
                current.next = self.first
                self.first = current
                current = previous.next
            elif current.value > x:
                if previous is None:
                    # right parts in the beginning
                    self.last.next = current
                    self.last = current
                    self.first = current.next
                    current = current.next
                    self.last.next = None
                    continue
                previous.next = current.next
                self.last.next = current
                self.last = current
                self.last.next = None
                current = previous.next
            else:
                # current.value == x
                previous = current
                current = current.next


L = LinkedList()
L.add(3)
L.add(5)
L.add(8)
L.add(5)
L.add(10)
L.add(2)
L.add(1)

print(L)
L.sift(5)
print(L)
