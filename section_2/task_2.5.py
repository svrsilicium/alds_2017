# -*- coding: UTF-8 -*-

"""
Два числа хранятся в виде связных списков, в которых каждый узел представляет один разряд.
Все цифры хранятся в обратном порядке, при этом младший разряд (единицы) хранится в начале
списка. Напишите функцию, которая суммирует два числа и возвращает результат в виде
связного списка.
Дополнительно
Решите задачу, предполагая, что цифры записаны в прямом порядке.
7, 30, 71, 95, 109
"""


# ~30 min,  O(n),   memory O(n)

class Node:
    def __init__(self, value=None, next=None):
        self.value = value
        self.next = next


class LinkedList:
    def __init__(self):
        self.first = None
        self.last = None
        self.length = 0

    def clear(self):
        self.__init__()

    def add(self, value):
        self.length += 1
        if self.first is None:
            self.first = self.last = Node(value)
        else:
            self.last.next = self.last = Node(value)

    def __str__(self):
        if self.length > 0:
            current = self.first
            out = "LinkedList [" + str(current.value)
            while current.next is not None:
                current = current.next
                out += ", " + str(current.value)
            return out + "]"
        else:
            return "LinkedList []"


def sum_two_numbers_reversed(num_1, num_2):
    buf = 0
    sum_list = LinkedList()

    if num_1.length >= num_2.length:
        size = num_1.length
        cur_n_1, cur_n_2 = num_1.first, num_2.first
    else:
        size = num_2.length
        cur_n_1, cur_n_2 = num_2.first, num_1.first

    for i in range(size):
        if cur_n_2 is not None:
            s1, s2 = cur_n_1.value, cur_n_2.value
        else:
            s1, s2 = cur_n_1.value, 0

        s = s1 + s2 + buf
        cur_sum, buf = s % 10, s // 10
        sum_list.add(cur_sum)

        cur_n_1 = cur_n_1.next
        cur_n_2 = cur_n_2.next if cur_n_2 is not None else None

    return sum_list


number_1 = LinkedList()
number_1.add(5)
number_1.add(9)
number_1.add(1)
number_1.add(1)
number_1.add(3)

number_2 = LinkedList()
number_2.add(9)
number_2.add(9)
number_2.add(2)

s0 = sum_two_numbers_reversed(number_1, number_2)    # 31195 + 299 = 31494
print(s0)                                            # out: 49413 -> 31494
