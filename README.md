# Algorithms and data structures

Project is divided by sections according with sections of the given part of the book.

For now, tasks from section 1 and 2 are fully done. Other sections - partially.

Tasks are done using Python 3 language (3.6). 
However, all the files, where it is not specifically indicated, are runnable with Python 2.7.   