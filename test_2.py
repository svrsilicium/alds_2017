import time


# O(n!)

fin = []

steps = 0


def are_all_unique(string):
    for i, x in enumerate(string):
        if string[i+1:].find(x) >= 0:
            return False
    return True


def compare(z, perm_1, k):
    for times in range(2, z + 1):
        perm_2 = str(int(perm_1) * times)
        print(perm_1, perm_2, times, k)
        if are_all_unique(perm_2) and perm_2.find("0") == -1:
            fin.append([perm_1, perm_2, times])


def permutation(values: list, k: int = 0):
    if k == len(values):
        perm = "".join(str(x) for x in values)
        if int(perm[0]) == 1:
            compare(8, perm, k)
        elif int(perm[0]) == 2:
            compare(4, perm, k)
        elif int(perm[0]) == 3:
            compare(3, perm, k)
        elif int(perm[0]) == 4:
            compare(2, perm, k)
    else:
        for i in range(k, len(values)):
            values[k], values[i] = values[i], values[k]
            permutation(values, k + 1)
            values[k], values[i] = values[i], values[k]


permutation([a for a in range(1, 10)])

print("count =", len(fin))
