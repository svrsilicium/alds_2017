# -*- coding: UTF-8 -*-

"""
Для заданного направленного графа разработайте алгоритм, проверяющий
существование маршрута между двумя узлами.
127
"""


# ~30 min,  not optimal by memory

# BFS
def search(start, end):
    visited = [start]
    queue = list()

    queue.append(start)

    while len(queue) != 0:
        cur = queue.pop(0)
        for i in edges[cur]:
            if i == end:
                return True
            if i not in visited:
                visited.append(i)
                queue.append(i)
    return False


edges = {
    0: [1, 2, 4],
    1: [2, 3, 5],
    2: [4],
    3: [6],
    4: [5, 7],
    5: [6],
    6: [2, 8],
    7: [],
    8: [9],
}

print(search(0, 9))
