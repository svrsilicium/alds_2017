# -*- coding: UTF-8 -*-

"""
Т1 и Т2 - два очень больших бинарных дерева, причем Т1 значительно больше Т2.
Создайте алгоритм, проверяющий, является ли т2 поддеревом Т1.
Дерево Т2 считается поддеревом Т1, если существует такой узел n в Т1, что
поддерево, "растущее" из n, идентично дереву Т2. (Иначе говоря, если вырезать дерево
в узле n, оно будет идентично Т2.)
Подсказки: 4+, 11+, 18, 31, 37
"""


# ~1 hr,    O(M*N)

class Node:
    def __init__(self, data=None):
        self.data = data
        self.left = None
        self.right = None


def equals(root_1, root_2):
    # equals only if both are None
    if root_1 is None and root_2 is None:
        return True
    elif root_1 is None or root_2 is None:
        return False
    # or datas are equal
    elif root_1.data == root_2.data \
            and equals(root_1.left, root_2.left) \
            and equals(root_1.right, root_2.right):
        return True
    else:
        return False


def is_subtree(t_2, t_1):
    if t_2 is None:
        return True

    if t_1 is None:
        return False

    if equals(t_2, t_1):
        return True

    # first left, then right
    return is_subtree(t_2, t_1.left) or is_subtree(t_2, t_1.right)


t_1 = Node(42)
t_1.right = Node(15)
t_1.right.left = Node(3)
t_1.right.right = Node(6)
t_1.left = Node(20)
t_1.left.left = Node(10)
t_1.left.left.right = Node(5)
t_1.left.right = Node(13)


t_2 = Node(20)
t_2.left = Node(10)
t_2.left.right = Node(5)
t_2.right = Node(13)

if is_subtree(t_2, t_1):
    print("Tree 2 is subtree of Tree 1")
else:
    print("Tree 2 is not a subtree of Tree 1")

t_1 = Node(42)
t_1.right = Node(15)
t_1.right.left = Node(3)
t_1.right.right = Node(6)
t_1.left = Node(20)
t_1.left.left = Node(10)
t_1.left.left.right = Node(5)
t_1.left.right = Node(13)


t_2 = Node(20)
t_2.left = Node(10)
t_2.left.right = Node(2)      # <-----  2 != 5
t_2.right = Node(13)

if is_subtree(t_2, t_1):
    print("Tree 2 is subtree of Tree 1")
else:
    print("Tree 2 is not a subtree of Tree 1")

t_1 = Node(42)
t_1.right = Node(15)
t_1.right.left = Node(3)
t_1.right.right = Node(6)
t_1.left = Node(20)
t_1.left.left = Node(10)
t_1.left.left.right = Node(5)
t_1.left.right = Node(13)

t_2 = Node()

if is_subtree(t_2, t_1):
    print("Tree 2 is subtree of Tree 1")
else:
    print("Tree 2 is not a subtree of Tree 1")