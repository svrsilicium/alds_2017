# -*- coding: UTF-8 -*-

"""
Имеется список проектов и список зависимостей (список пар проектов,
для которых проект зависит от второго проекта). Проект может быть
построентолько после построения всех его зависимостей. Найдите такой,
порядок построения, который позволит построить все проекты.
Если действительного порядка не существует, верните признак ошибки.
Пример:
Ввод:
    проекты: a, b, c, d, e, f
    зависимости: (d, a), (b, f), (d, b), (a, f), (c, d)
Вывод:
    f, e, a, b, d, c
Подсказки: 26, 47, 60, 85, 125, 133
"""


#

nodes = ["a", "b", "c", "d", "e", "f"]

edges = {
    "a": ["d"],
    "f": ["b", "a"],
    "b": ["d"],
    "d": ["c"],
    "c": [],
    "e": [],
}

color = {
    "a": 0,
    "b": 0,
    "c": 0,
    "d": 0,
    "e": 0,
    "f": 0,
}


def found_cycle(cur: str, edges):
    color[cur] = 1
    for i in edges[cur]:
        if color[i] == 0:
            found_cycle(i, edges)
        if color[i] == 1:
            print("Cycle!")
            return True
    color[cur] = 2
    return False


def algorithm(nodes, edges):
    for x in nodes:
        if found_cycle(x, edges):
            print("To bad")
            return False
    return True


nodes = ["a", "b", "c", "d"]

edges = {
    "a": ["b", "c"],
    "b": ["c"],
    "c": ["d"],
    "d": ["b"],
}

if algorithm(nodes, edges):
    print("OK")

# for x in edges.values():
