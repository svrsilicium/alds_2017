# -*- coding: UTF-8 -*-

"""
Для бинарного дерева разработайте алгоритм, который создаёт связный список всех узлов,
находящихся на каждой глубине (для дерева с глубиной D должно получиться D связных списков).
107, 123, 135
"""


# ~ 25 min,

class TreeNode:
    def __init__(self, value=None, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


class Node:
    def __init__(self, data: TreeNode = None, next=None):
        self.data = data
        self.next = next


class LinkedList:
    def __init__(self):
        self.first = None
        self.last = None
        self.length = 0

    def add(self, other):
        self.length += 1
        if self.first is None:
            self.first = self.last = Node(other)
        else:
            self.last.next = self.last = Node(other)

    def __str__(self):
        if self.first is not None:
            current = self.first
            out = "LinkedList [" + str(current.data.value)
            while current.next is not None:
                current = current.next
                out += ", " + str(current.data.value)
            return out + "]"
        return "LinkedList []"


lists = dict()  # dictionary {depth: LinkedList,}


def add_to_list(node: TreeNode, depth: int):
    if depth not in lists.keys():
        lists[depth] = LinkedList()
    lists[depth].add(node)


def pre_order_traversal(node: TreeNode, depth: int):
    if node is not None:
        add_to_list(node, depth)
        pre_order_traversal(node.left, depth + 1)
        pre_order_traversal(node.right, depth + 1)


def algorithm(root: TreeNode):
    depth = 0
    pre_order_traversal(root, depth)


n1 = TreeNode(1)
n2 = TreeNode(2)
n3 = TreeNode(3, n1, n2)
n7 = TreeNode(7)
n9 = TreeNode(9)
n8 = TreeNode(8, right=n9)
n6 = TreeNode(6, n7, n8)
n5 = TreeNode(5, n3, n6)

algorithm(n5)

print(lists)
for x, y in lists.items():
    print("depth = {:d}, {:}".format(x, y))

