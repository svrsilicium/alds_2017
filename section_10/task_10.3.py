# -*- coding: UTF-8 -*-

"""
Имеется отсортированный массив из N целых чисел, который был циклически сдвинут
неизвестное колличество раз. Напишите код для поиска элемента в массиве. Предполагается,
что массив изначально был отсортирован по возрастанию.
Пример:
Ввод: найти 5 в {15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14}
Вывод: 8 (индекс числа 5 в массиве)
"""


# ~ 40 min,     O(N*logN)

def search(arr: list, x: int, left: int, right: int):
    half = (left + right) // 2

    if x == arr[half]:
        return half
    elif right < left:
        return "Not found"

    if arr[left] < arr[half]:                                   # left half is sorted
        if arr[left] <= x < arr[half]:                          # and x in it
            return search(arr, x, left, half - 1)
        else:
            return search(arr, x, half + 1, right)              # x in right half

    elif arr[left] > arr[half]:                                 # left half is moved
        if arr[half + 1] <= x <= arr[right]:                    # and x in right part
            return search(arr, x, half + 1, right)
        else:
            return search(arr, x, left, half - 1)               # or x in left

    elif arr[left] == arr[half]:                                # duplicates
        if arr[half] != arr[right]:                             # x in right part
            return search(arr, x, half + 1, right)
        else:
            result = search(arr, x, left, half - 1)             # x in left
            if result == "Not found":
                return search(arr, x, half + 1, right)
            else:
                return result
    return "Not found"


array = [15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14]

print(search(array, 5, 0, len(array) - 1))
