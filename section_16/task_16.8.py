"""
Задано целое число. Выведите его значение в текстовом виде
(например, "одна тысяча двести тридцать четыре").
Подсказки: 502, 588, 688
"""


digits = {
    1: "один",
    2: "два",
    3: "три",
    4: "четыре",
    5: "пять",
    6: "шесть",
    7: "семь",
    8: "восемь",
    9: "девять",
}
tens = {
    2: "двадцать",
    3: "тридцать",
    4: "сорок",
    5: "пятьдесят",
    6: "шестьдесят",
    7: "семьдесят",
    8: "восемьдесят",
    9: "девяносто",
}
teens = {
    0: "десять",
    1: "одинадцать",
    2: "двенадцать",
    3: "тринадцать",
    4: "четырнадцать",
    5: "пятнадцать",
    6: "шестнадцать",
    7: "семнадцать",
    8: "восемнадцать",
    9: "девятнадцать",
}
hundreds = {
    1: "сто",
    2: "двести",
    3: "триста",
    4: "четыреста",
    5: "пятьсот",
    6: "шестьсот",
    7: "семьсот",
    8: "восемьсот",
    9: "девятьсот",
}

bils = {1: "миллиард", 2: "миллиарда", 3: "миллиардов"}
mils = {1: "миллион", 2: "миллиона", 3: "миллионов"}
ths = {1: "тысяча", 2: "тысячи", 3: "тысяч"}

names = {3: bils, 2: mils, 1: ths, 0: ""}


def to_text3(n: int) -> str:
    string = ""

    # reversed number in string
    n_str = str(n)[::-1]

    # storing every 3 digits separately
    num = dict()
    base = (len(n_str) + 2) // 3
    for i in range(base):
        num[i] = n_str[i*3:i*3+3][::-1]

    # for every 3 digits
    for k in range(base - 1, -1, -1):
        cur = int(num[k])
        big = ""

        # russian declensions of counts
        if k != 0:
            if cur % 10 == 1:
                big = names[k][1]
                if cur % 100 == 11:
                    big = names[k][3]
            elif 2 <= cur % 10 <= 4:
                big = names[k][2]
                if 12 <= cur % 100 <= 14:
                    big = names[k][3]
            elif cur % 100 > 4:
                big = names[k][3]

        string += to_text_100(cur, k) + " " + big + " "
    return string


def to_text_100(n: int, k: int) -> str:
    string = ""

    # hundreds
    if n >= 100:
        string += hundreds[n // 100] + " "

    # tens and teens
    n = n % 100
    if 20 <= n < 100:
        string += tens[n // 10] + " "
    elif 10 <= n <= 19:
        return string + teens[n - 10]

    # digits
    n = n % 10
    if 1 <= n <= 10:
        digit = digits[n]
        if k == 1 and digit == 'один':
            digit = 'одна'
        elif k == 1 and digit == 'два':
            digit = 'две'
        string += digit

    return string


print(1, 234, 561, 896)
print(to_text3(1234561896))

print()
print(11, 234, 171, 800)
print(to_text3(11234171800))

print()
print(117, 540)
print(to_text3(117540))
