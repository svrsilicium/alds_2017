"""
Напишите функцию, которая переставляет значения переменных "на месте"
(то есть без использования временных переменных).
Подсказки: 492+, 716+, 737+
"""


# 15 min

# in python it's pretty simple

a = 10
b = 42

a, b = b, a

print(a, b)     # 42 , 10


# standard solution
def swap(a: int, b: int) -> (int, int):
    """
    in Python one don't need to check whether values exceed size of a type
    but in other language it could be necessary to check
    """
    a = a + b   # 52
    b = a - b   # 42
    a = a - b   # 10
    return a, b


print(swap(10, 42))     # 42 , 10


# using XOR
def swap_xor(a: int, b: int) -> (int, int):
    a = a ^ b
    b = b ^ a
    a = a ^ b
    return a, b


print(swap_xor(10, 42))     # 42 , 10
