"""
Напишите метод, находящий максимальное из двух чисел без использования
if-else или любых других операторов сравнения.
Подсказки: 473, 513, 707, 728
"""


# 30 min

def maximum(a: int, b: int) -> int:
    return int((a + b + abs(a - b)) / 2)


print(maximum(100, 43))     # 100
print(maximum(-81, 27))     # 27
print(maximum(-81, -127))   # -81

print()


def maximum(a: int, b: int) -> int:
    sign = ((a - b) >> 31) + 1
    return a*sign + b*(not sign)


print(maximum(10, 42))      # 42
print(maximum(10, -42))     # 10
print(maximum(-10, -42))    # -10
