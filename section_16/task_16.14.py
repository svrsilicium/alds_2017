"""
Для заданного набора точек на плоскости найдите линию, проходящую через
максимальное количество точек.
Подсказки: 491, 520, 529, 563
"""

# more than 1 hr,    O(n^2)

import math


class Point:
    def __init__(self, x: int = None, y: int = None):
        self.x = x
        self.y = y

    def __str__(self):
        return "Point(" + str(self.x) + ", " + str(self.y) + ")"

    def __repr__(self):
        return "Point(" + str(self.x) + ", " + str(self.y) + ")"


class Line:
    a = None
    b = None
    c = None

    def __init__(self, coeffs: list):
        self.a = coeffs[0]
        self.b = coeffs[1]
        self.c = coeffs[2]

    def __str__(self):
        if self.a is not None:
            out = "Line [" + \
                  str(self.a) + ", " + \
                  str(self.b) + ", " + \
                  str(self.c) + "]"
            return out
        return "Line []"

    def __eq__(self, other):
        """Override the default Equals behavior"""
        return self.a == other.a and self.b == other.b and self.c == other.c

    def __ne__(self, other):
        """Override the default Unequal behavior"""
        return self.a != other.a or self.b != other.b or self.c != other.c

    def __hash__(self):
        return hash((self.a, self.b, self.c))


points = [
    Point(1, 1),
    Point(2, 3),
    Point(2, 6),
    Point(3, 2),
    Point(3, 5),
    Point(5, 2),
    Point(5, 3),
    Point(8, 2),
    Point(9, 5),
]

# ---------- main part ----------
lines = dict()      # {ind: Line}


def get_line_coef(p1: Point, p2: Point) -> list:
    """
    Returns A, B and C coefficients of Ax + By + C = 0 line equation
    :param p1: first Point
    :param p2: second Point
    """
    a = p2.y - p1.y
    b = p1.x - p2.x
    c = p1.y*p2.x - p1.x*p2.y
    if a < 0:
        a = -a
        b = -b
        c = -c
    x = math.gcd(a, b)
    x = math.gcd(c, x)
    return [a / x, b / x, c / x]


for i, x in enumerate(points[:-1]):
    for y in points[i + 1:]:
        cur = Line(get_line_coef(x, y))
        if cur in lines:
            if y not in lines[cur]:
                lines[cur].append(y)
        else:
            lines[cur] = [x, y]
# ---------- main part ----------


# printing
for x, y in lines.items():
    print(x, y)

print(len(lines))
print(max(lines.keys(), key=(lambda k: len(lines[k]))))     # Line [1.0, -2.0, 1.0]
