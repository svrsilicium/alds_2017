"""
Разметка XML занимает слишком много места. Закодируйте ее так, чтобы
каждый тег заменялся заранее определённым целым значением:

    Элемент --> Тег Атрибуты END ДочерниеТеги END
    Атрибут --> Тег Значение
    END --> 0
    Тег --> любое предопределенное целое значение
    Значение --> строка значение END

Для примера преобразуем в сжатую форму следующую разметку XML.
Предполагается, что family -> 1, person -> 2, firstName -> 3, lastName -> 4,
state -> 5:

    <family lastName="McDowell" state="CA">
        <person firstName="Gayle">Some Message</person>
    </family>

После пробразования код будет иметь вид:
    1 4 McDowell 5 CA 0 2 3 Gayle 0 Some Message 0 0

Напишите код, выводящий закодированную версию XML-элемента (переданного
в объектах Element и Attributes).
Подсказки: 466
"""

import sys

string = """
    <family lastName="McDowell" state="CA">
        <person firstName="Gayle">Some Message</person>
    </family>
"""

tags = {
    'family': '1',
    'person': '2',
}

att_names = {
    'firstName': '3',
    'lastName': '4',
    'state': '5',
}

final_string = ""


def foo():
    global string
    string = string.replace("  ", "").replace("\n", "")
    print(string)

    x = get_next()
    print(x, string)

    x = get_next()
    print(x, string)


def element():
    tag()
    attributes()
    end()
    element()
    end()


special_symbols = ['<', '>', '"', '=', '/']


def get_next() -> str:
    """
    Retrieving special symbol or word, except spaces
    :return: symbol or word
    """
    global string
    current = string[0]
    if current in special_symbols:
        string = string[1:]
        return current
    elif current == ' ':
        string = string[1:]
        return get_next()
    else:
        fin = ""
        while current not in special_symbols and current != ' ':
            fin += current
            string = string[1:]
            current = string[0]
        return fin


def tag():
    global final_string
    if get_next() == '<':
        tag = get_next()
        if tag in tags:
            tag = tags[tag]
            final_string += tag + " "
    else:
        sys.exit("not a tag")


def attributes():
    attribute()
    attribute_or_none()


def attribute_or_none():
    pass


def att_value():
    pass


def attribute():
    global final_string
    att_name = get_next()
    if get_next() == "=":
        att_name = att_names[att_name]
        final_string += att_name
    att_value()


def end():
    pass


foo()
