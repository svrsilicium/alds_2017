"""
Разметка XML занимает слишком много места. Закодируйте ее так, чтобы
каждый тег заменялся заранее определённым целым значением:

    Элемент --> Тег Атрибуты END ДочерниеТеги END
    Атрибут --> Тег Значение
    END --> 0
    Тег --> любое предопределенное целое значение
    Значение --> строка значение END

Для примера преобразуем в сжатую форму следующую разметку XML.
Предполагается, что family -> 1, person -> 2, firstName -> 3, lastName -> 4,
state -> 5:

    <family lastName="McDowell" state="CA">
        <person firstName="Gayle">Some Message</person>
    </family>

После пробразования код будет иметь вид:
    1 4 McDowell 5 CA 0 2 3 Gayle 0 Some Message 0 0

Напишите код, выводящий закодированную версию XML-элемента (переданного
в объектах Element и Attributes).
Подсказки: 466
"""

string = """
    <family lastName="McDowell" state="CA">
        <person firstName="Gayle">Some Message</person>
    </family>
"""

tags = ['family', 'person']
atts = ['lastName', 'firstName', 'state']

rep_dict = {
    'family': '1',
    'person': '2',
    'firstName': '3',
    'lastName': '4',
    'state': '5',
}

replacements = dict()

for x in tags:
    replacements['<' + x] = rep_dict[x]
    replacements['</' + x] = ' 0'

for y in atts:
    replacements[y + '='] = rep_dict[y]


def foo(string: str) -> str:
    string = string.replace("  ", "").replace("\n", "")
    for x, y in replacements.items():
        string = string.replace(x, y)
    string = string.replace('"', " ").replace(">", "")
    return string


print(foo(string))
