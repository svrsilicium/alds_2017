"""
Разработайте алгоритм для поиска в массиве всех пар целых чисел,
сумма которых равна заданному значению.
Подсказки: 548, 597, 644, 673
"""


# 30 min, O(n),     memory O(n)

from random import *


array = [randint(1, 10) for x in range(10)]
print(array)

wanted = randint(1, 15)
nums = {}
pairs = list()

for x in array:
    if wanted - x not in nums:
        nums[x] = x
    else:
        pairs.append((x, wanted - x))

print("wanted: {:d}".format(wanted))
print(pairs)
