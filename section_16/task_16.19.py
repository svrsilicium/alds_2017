"""
Целочисленная матрица представляет участок земли; значение каждого
элемента матрицы обозначает высоту над уровнем моря. Нулевые элементы
представляют воду. Назовем "озером" область водных участков, связанных
по вертикали, горизонтали или диагонали. Размер озера равен общему
количеству соединённых водных участков. Напишите метод для вычисления
размеров всех озер в матрице.
Пример:
    Ввод:
        0 2 1 0
        0 1 0 1
        1 1 0 1
        0 1 0 1
    Вывод:
        2, 4, 1 (в любом порядке)
Подсказки: 674, 687, 706, 723
"""


# more than 1 hr,   O(N^2)

# import numpy as np
# import matplotlib.pyplot as plt
#
# import matplotlib.pyplot as plt
# from mpl_toolkits.axes_grid1 import make_axes_locatable
# import numpy as np


class Point:
    def __init__(self, value=None, visited=False):
        self.value = value
        self.visited = visited


field = [
    [Point(0), Point(2), Point(1), Point(0)],
    [Point(0), Point(1), Point(0), Point(1)],
    [Point(1), Point(1), Point(0), Point(1)],
    [Point(0), Point(1), Point(0), Point(1)],
]

# field = [
#     [Point(0), Point(1), Point(1), Point(0), Point(1), Point(1), Point(1), Point(1), Point(0), Point(1)],
#     [Point(0), Point(1), Point(0), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1)],
#     [Point(1), Point(1), Point(0), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1)],
#     [Point(0), Point(1), Point(0), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1)],
#     [Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(0), Point(1)],
#     [Point(1), Point(0), Point(1), Point(0), Point(0), Point(1), Point(1), Point(1), Point(0), Point(1)],
#     [Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(0), Point(0), Point(1)],
#     [Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(0), Point(1), Point(1)],
#     [Point(1), Point(1), Point(1), Point(1), Point(1), Point(0), Point(1), Point(1), Point(0), Point(1)],
#     [Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1), Point(1)],
# ]


FIELD_X = len(field)
FIELD_Y = len(field[0])

lakes = dict()
steps = 0


def search(field):
    """
    For every unvisited cell:
    if 0 then check it neighbours, else make it visited
    """
    # global steps

    count = 0
    for x in range(FIELD_X):
        for y in range(FIELD_Y):
            if field[x][y].visited:
                continue
            else:
                steps += 1
                if field[x][y].value == 0:
                    lakes[count] = 1
                    field[x][y].visited = True
                    check_neighbours(x, y, field, count)
                    count += 1
                else:
                    field[x][y].visited = True


def check_neighbours(x: int, y: int, field: list, count: int):
    """
    Checking cells around given, if 0 recursively check its neighbours,
    if not 0 make it visited
    """

    global steps

    for i in [-1, 0, 1]:
        for j in [-1, 0, 1]:
            if 0 <= x + i < FIELD_X and 0 <= y + j < FIELD_Y:
                if field[x + i][y + j].visited:
                    continue
                else:
                    steps += 1
                    if field[x + i][y + j].value == 0:
                        lakes[count] += 1
                        field[x + i][y + j].visited = True
                        check_neighbours(x + i, y + j, field, count)
                    else:
                        field[x + i][y + j].visited = True


search(field)
print(lakes)
print("{:d} steps for {:d} x {:d}".format(steps, FIELD_X, FIELD_Y))


# # some visualization
# # heatmap = plt.pcolor(field)
#
#
# ax = plt.subplot(111)
# im = ax.imshow(np.array([y.value for x in field for y in x]).reshape((FIELD_X, FIELD_Y)))
#
# # create an axes on the right side of ax. The width of cax will be 5%
# # of ax and the padding between cax and ax will be fixed at 0.05 inch.
# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.05)
#
# plt.colorbar(im, cax=cax)
# plt.show()
