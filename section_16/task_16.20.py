""""
На старых сотовых телефонах при наборе текста пользователь нажимал клавиши
на цифровой клавиатуре, а телефон выдавал список слов, соответствующих
введенным цифрам. Каждая цифра представляла набор от 0 до 4 цифр.
Реализуйте алгоритм для получения списка слов, соответствующих заданной
последовательности цифр. Предполагается, что список допустимых слов задан
(в любой структуре данных на ваш выбор). Следующая диаграмма поясняет
процесс подбора:
                    1    2    3
                        abc  def
                    4    5    6
                   ghi  jkl  mno
                    7    8    9
                   pqrs tuv  wxyz
                         0
Пример:
    Ввод: 8733
    Вывод: tree, used
Подсказки: 471, 487, 654, 703, 726, 744
"""

# 20 min,   O(1)

# ---- preparation stage ----
dictionary = {
    't': ['twin', 'tree', 'total'],
    'u': ['under', 'up', 'used', 'utmost'],
    'a': ['alphabet', 'anchor'],
    'b': ['brown', 'byte', 'bit'],
}

keyboard = {
    'a': 2,
    'b': 2,
    'c': 2,

    'd': 3,
    'e': 3,
    'f': 3,

    'g': 4,
    'h': 4,
    'i': 4,

    'j': 5,
    'k': 5,
    'l': 5,

    'm': 6,
    'n': 6,
    'o': 6,

    'p': 7,
    'q': 7,
    'r': 7,
    's': 7,

    't': 8,
    'u': 8,
    'v': 8,

    'w': 9,
    'x': 9,
    'y': 9,
    'z': 9,

}

keywords = dict()

for d in dictionary.values():
    for word in d:
        i = 0
        code = ""
        while i < len(word):
            code += str(keyboard[word[i]])
            i += 1
        if int(code) not in keywords.keys():
            keywords[int(code)] = [word]
        else:
            keywords[int(code)].append(word)
# ---- preparation stage end ----

print(keywords)


def find_words(code: int) -> list:
    return keywords[code]


print("Entered: 8733")
print("Words: {:}".format(find_words(8733)))
