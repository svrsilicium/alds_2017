"""
Имеется массив целых чисел (как положительных, так и отрицательных).
Найдите непрерывную последовательность с наибольшей суммой. Верните
найденную сумму.
Пример:
    Ввод: 2, -8, 3, -2, 4, -10
    Вывод: 5 (соответствующая последовательность: 3, -2, 4)
Подсказки: 531, 551, 567, 594, 614
"""


# ~ 25 min,     O(n)

def find_maximum_sequence(array: list) -> (int, list):
    maximum = 0
    summa = 0
    sequence = []
    max_sequence = []
    for x in array:
        summa += x
        sequence.append(x)
        if summa > maximum:
            maximum = summa
            max_sequence = sequence.copy()
        elif summa < 0:
            summa = 0
            sequence = []
    return maximum, max_sequence


array = [2, -8, 3, -2, 4, -10]
print(find_maximum_sequence(array))     # 5, [3, -2, 4]

array = [-2, 5, -3, -1, 2, 3, -4, 1]
print(find_maximum_sequence(array))     # 6, [5, -3, -1, 2, 3]
