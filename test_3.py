from pprint import pprint


# O(n^2)
# it's meant that courses are sorted by start day
# could be optimized to not recursively run for courses that starts after the minimum one

steps = 0


def search(start, arr: list, res):
    """
    For every course that starts after the end of previous one
    Could be optimized a bit
    """
    global steps
    for course in arr:
        for index, days in course.items():
            if days[0] > start:
                steps += 1
                new_dict = dict()
                res[index] = new_dict   # 1
                search(days[-1], arr[1:], new_dict)


# courses = [
#     {1: [1, 2]},
#     {2: [2, 3]},
#     {3: [4, 5]},
#     {4: [5, 6]},
#     {5: [6, 7]},
#     {6: [8, 9]},
#     {7: [9, 10]},
# ]

# courses = [
#     {1: [1, 2]},
#     {2: [2, 3]},
#     {3: [3, 4]},
#     {4: [4, 5]},
#     {5: [5, 6]},
#     {6: [6, 7]},
#     {7: [7, 8]},
#     {8: [8, 9]},
#     {9: [9, 10]},
# ]

courses = [
    {101: [1, 2, 3, 4]},
    {102: [2, 3, 4, 5]},
    {103: [4, 5]},
    {104: [5, 6, 7]},
    {105: [6, 7, 8, 9]},
    {106: [8, 9, 10]},
    {107: [10, 11]},
]

# kinda tree represented by dict od dicts*
result = dict()

search(0, courses, result)
pprint(result)
print("Steps: {:d} for {:d} courses".format(steps, len(courses)))

final_list = []


def list_from_dicts(given_dict: dict, final: list) -> list:
    """
    Create list from dict of dicts*
    """
    def append(m, t):
        if len(m) == 0:
            m.append(t)
            return 0
        else:
            for i in m:
                i.append(t)
            return 1

    def dig(given_dict: dict, depth: int):
        depth += 1
        if len(given_dict) == 0:
            return []
        else:
            temp_list = []
            for index, sub_dict in given_dict.items():
                buf = dig(sub_dict, depth)
                flag = append(buf, index)
                if depth == 1:
                    temp_list.extend(buf) if len(buf) != 1 else temp_list.append(buf)
                elif flag == 0:
                    temp_list.append(buf)
                elif temp_list != buf:
                    temp_list.extend(buf)
            return temp_list

    final.extend(dig(given_dict, depth=0))
    return final


def get_schedule():
    """
    Just for printing
    """
    res = list_from_dicts(result, final_list)

    m = max([len(x) for x in res])

    fin = []
    for x in res:
        if len(x) == m:
            fin.append(x)
    return fin


print("Courses one can visit (index : [days]):")
pprint(courses)
print("Course indexes to visit (variants) (not sorted): {:}".format(get_schedule()))
