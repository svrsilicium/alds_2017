"""
Разработайте алгоритм для поиска наименьших k чисел в массиве.
Подсказки: 470, 530, 552, 593, 625, 647, 661, 678
"""

from random import *




# def sort(arr: list, k: int):
#     if len(arr) == 1 or len(arr) == 0:
#         return arr
#     else:
#         pivot_pos = randint(0, len(arr) - 1)
#         pivot = arr[pivot_pos]
#         print("pivot: {:d} at position: {:d}".format(pivot, pivot_pos))
#         print(arr)
#         i = 0
#         for j in range(len(arr) - 1):
#             if arr[j + 1] < pivot:
#                 arr[j + 1], arr[i + 1] = arr[i + 1], arr[j + 1]
#                 i += 1
#         arr[pivot_pos], arr[i] = arr[i], arr[pivot_pos]
#         print(arr)


def sort(arr, left, right, k):
    index = partition(arr, left, right, k)
    if index == k - 1:
        return array[:k]
    else:
        sort(arr, left, right, k)
    # if index == k or index + 1 == k:
    #     return arr[:k]
    # elif index > k:
    #     sort(arr, left, index - 1, k)
    # elif index < k - 1:
    #     sort(arr, index + 1, right, k)
    # if left < index - 1:
    #     sort(arr, left, index - 1, k)
    # if index < right:
    #     sort(arr, index, right, k)
    return arr[:k]


def partition(arr, left_pos, right_pos, k):
    # pivot_pos = (left_pos + right_pos) // 2
    pivot_pos = k - 1
    pivot = arr[pivot_pos]
    print(arr)
    print("pivot: {:d} at position: {:d}".format(pivot, pivot_pos))

    while left_pos <= right_pos:
        left = arr[left_pos]
        while left < pivot:        # todo equal elements
            left_pos += 1
            left = arr[left_pos]
        right = arr[right_pos]
        while right >= pivot and right_pos != 0:
            right_pos -= 1
            right = arr[right_pos]

        if left_pos <= right_pos:
            arr[left_pos], arr[right_pos] = arr[right_pos], arr[left_pos]
            if pivot_pos == left_pos:
                pivot_pos = right_pos
            elif pivot_pos == right_pos:
                pivot_pos = left_pos
            left_pos += 1
            right_pos -= 1
        print(arr)
    return pivot_pos
#
#
# def sort(arr, left_pos, right_pos, k):
#     pivot_pos = 4
#     pivot = arr[pivot_pos]
#
#     while left_pos < pivot_pos and right_pos > pivot_pos:
#         left = arr[left_pos]
#         while left < pivot:
#             left_pos += 1
#             left = arr[left_pos]
#
#         right = arr[right_pos]
#         while right >= pivot:
#             right_pos -= 1
#             right = arr[right_pos]
#
#         arr[left_pos], arr[right_pos] = arr[right_pos], arr[left_pos]
#         left_pos += 1
#         right_pos -= 1
#         print(arr)
#
#     if left_pos == pivot_pos:
#         # continue_sort(arr, left_pos, right_pos, k)   # for right
#         pass
#     elif right_pos == pivot_pos + 1:
#         continue_sort()  # for left
#
#
# def continue_sort(arr, left_pos, right_pos, k):
#     pass



# array = [randint(1, 10) for x in range(15)]
array = [6, 9, 4, 8, 6, 2, 3, 8, 7, 6, 6, 10, 7, 1, 1]
print(array)

# k = randint(1, 3)
k = 4
print("k:", k)
print(array)

print(sort(array, 0, len(array) - 1, k))
print(array)


# sort(array, k)
