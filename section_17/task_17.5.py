"""
Имеется массив, заполненный буквами и цифрами. Найдите самый длинный
подмассив с равным количеством букв и цифр.
Подсказки: 485+, 515, 619, 671, 713
"""


# ~ more than 1 hr,     O(N)    memory O(1)

def algorithm(array: list) -> list:
    int_count = 0
    str_count = 0
    int_first_pos = None
    int_last_pos = None
    str_first_pos = None
    str_last_pos = None

    # storing first and last occurrences and total number of elements
    for i, x in enumerate(array):
        if isinstance(x, int):
            int_count += 1
            int_last_pos = i
            if int_count == 1:
                int_first_pos = i
        elif isinstance(x, str):
            str_count += 1
            str_last_pos = i
            if str_count == 1:
                str_first_pos = i

    if int_count == 0 or str_count == 0:
        return []
    elif int_count == 1:
        if int_first_pos == 0:
            return array[:2]
        if int_last_pos == len(array) - 1:
            return array[-2:]
    elif str_count == 1:
        if str_first_pos == 0:
            return array[:2]
        if str_last_pos == len(array) - 1:
            return array[-2:]

    # if number of elements are similar, then return whole array
    if int_count == str_count:
        return array
    elif int_count < str_count:
        return sub_algorithm(int_first_pos, int_last_pos, int_count, array)
    elif int_count > str_count:
        return sub_algorithm(str_first_pos, str_last_pos, str_count, array)


def sub_algorithm(min_first: int, min_last: int, min_count: int, array: list) -> list:
    # number of majority elements between minority boundary elements
    maj_between = min_last - min_first - min_count + 1

    if maj_between <= min_count:
        # number of extra majority elements that are not between minority boundary elements
        maj_extra = min_count - maj_between
        maj_left = min_first - maj_extra
        if maj_left >= 0:
            return array[maj_left:min_last + 1]
        else:
            return array[:min_last + abs(maj_left) + 1]
    elif maj_between > min_count:
        a1 = algorithm(array[min_first:min_last])
        a2 = algorithm(array[min_first + 1:min_last + 1])
        return a1 if len(a1) >= len(a2) else a2


array = [3, 7, 'b', 9, 'y', 'o', 11, 'p', 't', 8, 'i', 'l']
print("\nneed:", [3, 7, 'b', 9, 'y', 'o', 11, 'p', 't', 8])
print("have:", algorithm(array))

array = [3, 'b', 'y', 'o', 'p', 8, 7, 4, 2, 1, 6, 8, 9]
print("\nneed:", [3, 'b', 'y', 'o', 'p', 8, 7, 4])
print("have:", algorithm(array))

array = [1, 3, 6, 8, 9, 'b', 'y', 'o', 'p', 8, 7, 4, 2, 1]
print("\nneed:", [3, 6, 8, 9, 'b', 'y', 'o', 'p'])
print("have:", algorithm(array))

array = [1, 1, 1, 'o', 1, 'a', 'o', 'o', 1, 1, 1, 1, 1, 1, 'b', 1, 'o', 1, 1, 1]
print("\nneed:", ['o', 1, 'a', 'o', 'o', 1, 1, 1])
print("have:", algorithm(array))

array = [1, 1, 1, 'o', 1, 'a', 1, 1, 1, 1, 1, 1, 1, 'o', 1, 'b', 'o', 1, 1]
print("\nneed:", [1, 1, 'o', 1, 'b', 'o'])
print("have:", algorithm(array))

