"""
Правительство ежегодно публикует список 10000 самых распространённых имён,
выбираемых родителями, и их частоты (количество детей с каждым именем).
Единственная проблема заключается в том, что некоторые имена существуют в
нескольких вариантах написания. Например, "John" и "Jon" - фактически одно имя,
но в списке они будут занимать две разные позиции. Для двух заданных списков
(один содержит имена/частоты, другой - пары эквивалентных имен) напишите
алгоритм для вывода нового списка истинной частоты каждого имени. Обратите внимание:
если John и Jon являются синонимами и при этом Jon b Johny являются синонимами,
то и John и Johny являются синонимами (отношение транзитивно и симметрично).
В окончательном списке в качестве "настоящего" может использоваться любое имя.
Пример:
    Ввод:
        Имена: John (15), Jon (12), Chris (13), Kris (4), Christopher (19)
        Синонимы: (John, Jon), (John, Johny), (Chris, Kris), (Chris, Christopher)
    Вывод:
        John (27), Kris (36)
Подсказки: 478, 493, 512, 537, 586, 605, 655, 675, 704
"""


# ~ 1 hr,   O(n)    memory O(n)

# Solution:  Using a hash table with names as keys and pointers to objects of class Name,
#            that have next attributes: name (any of synonyms), list of synonyms and count
#            and custom representation (printing) methods
#            For better convenience I store name:Name in list for further printout

statistic = [("John", 15), ("Jon", 12), ("Chris", 13), ("Kris", 4), ("Christopher", 19), ("Jacky", 3)]

synonyms = [
    ("John", "Jon"),
    ("John", "Johny"),
    ("Chris", "Kris"),
    ("Chris", "Christopher")
]


class Name:
    def __init__(self, name: str = None, synonyms: list = list(), count: int = 0):
        self.name = name
        self.synonyms = synonyms
        self.count = count

    def __repr__(self):
        return "{:s}: {:d}".format(self.name, self.count)

    def __str__(self):
        return "{:}".format(self.synonyms)


def actual_statistic(synonyms: list, statistic: list) -> list:
    buf_dict = dict()
    names_list = []

    for synonym in synonyms:
        # if none of synonyms is in buf dictionary, create a new name link
        # add both synonyms to buf dictionary, make them point to real name
        if synonym[0] not in buf_dict.keys() and synonym[1] not in buf_dict.keys():
            name = Name(synonym[0], [synonym[0], synonym[1]])
            names_list.append(name)
            buf_dict[synonym[0]] = buf_dict[synonym[1]] = name
        # if one synonym is already in buf dictionary, add another to temp dictionary
        # and make it to point to existing name
        elif synonym[0] in buf_dict.keys():
            buf_dict[synonym[0]].synonyms.append(synonym[1])
            buf_dict[synonym[1]] = buf_dict[synonym[0]]
        elif synonym[1] in buf_dict.keys():
            buf_dict[synonym[1]].synonyms.append(synonym[0])
            buf_dict[synonym[0]] = buf_dict[synonym[1]]

    # for each pair of (name, count) add count to real name,
    # by using synonym name from buf dictionary
    for name_stat in statistic:
        if name_stat[0] in buf_dict.keys():
            buf_dict[name_stat[0]].count += name_stat[1]
        else:
            # if there wasn't synonyms, then just add statistic (make new Name)
            name_without_synonyms = Name(name=name_stat[0], count=name_stat[1])
            buf_dict[name_stat[0]] = name_without_synonyms
            names_list.append(name_without_synonyms)

    return names_list


print(actual_statistic(synonyms, statistic))
