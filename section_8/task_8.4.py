# -*- coding: UTF-8 -*-

"""
Напишите алгоритм, возвращающий все подмножества заданного множества.
273, 290, 338, 354, 373
"""


# ~1.5 hr

def c_n_k(n: int, k: int) -> int:
    return int(fact(n) / (fact(n-k) * fact(k)))


def fact(a: int) -> int:
    i, fact = 0, 1
    while i < a:
        i += 1
        fact = fact * i
    return fact


def algorithm(a: list) -> list:
    count, size = 0, len(a)
    b = []
    # getting count of subarrays
    for k in range(1, size):
        count += c_n_k(size, k)

    # knowing count of subarrays, iterating over them, representing each as reversed binary
    # bits of reversed binary are markers of array elements,
    # that are included or not in final subarray
    for i in range(count):
        c = []
        for j, x in enumerate(str(bin(i + 1))[2:][::-1]):   # reversed binary
            c.append(a[j]) if x == '1' else None
        b.append(c)
    return b


a = ["a", "b", "c", "d"]
print(algorithm(a))
